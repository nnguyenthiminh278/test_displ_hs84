import numpy as np


def get_data(file, query):
    result, N = [], -1
    with open(file) as reader:
        found = False
        for line in reader:
            if found and N != len(result):
                for num in list(filter(None, line.split())):
                    result.append(float(num))
            if query in line:
                data = list(filter(None, line.split()))
                N = int(data[data.index('N=') + 1])
                found = True
    return np.array(result)


# def read_limit_precision(value, p=3):
#     format = f"%.{p}f"
#     return float(format % (float(value), ))


def get_frequency(file, query):
    freqs = []
    with open(file) as reader:
        for line in filter(lambda x: query in x, reader.readlines()):
            data = list(filter(None, line.strip().split('Frequencies --')))
            data = list(filter(None, data[0].split()))
            freqs.extend([float(x) for x in data])
            # freqs.extend([read_limit_precision(x, p=0) for x in data])
    return np.array(freqs)


def get_wj_delta(file):
    delta, wj = [], []
    with open(file) as f:
        for line in f.readlines()[3:-1]:
            _, d, w, _ = line.split('&')
            # print(line.split('&'))
            d = d.replace('\\bf{', '').replace('}', '')
            w = w.replace('\\bf{', '').replace('}', '')
            delta.append(float(d))
            wj.append(float(w))
    return np.array(delta), np.array(wj)


if __name__ == '__main__':
    file_coordinate_GS = 'inputs/HS84_GS.fchk'  # CHANGE FOR OTHER MOLECULE
    file_coordinate_ES = 'inputs/HS84_ES.fchk'  # CHANGE FOR OTHER MOLECULE
    file_coordinate_FREQ = 'inputs/HS84_ES.log'  # CHANGE FOR OTHER MOLECULE
    file_harmonic_displacement = 'inputs/4_ranking.txt'  # CHANGE FOR OTHER MOLECULE
    file_output = 'outputs/output.txt'
    CM1_AU = 4.5563353e-6

    GS = get_data(file_coordinate_GS, 'Current cartesian coordinates')
    ES = get_data(file_coordinate_ES, 'Current cartesian coordinates')
    mass = np.sqrt(get_data(file_coordinate_ES, 'Vib-AtMass') * 1822.88848)
    nm = get_data(file_coordinate_ES, 'Vib-Modes')
    freqs = get_frequency(file_coordinate_FREQ, 'Frequencies --')
    delta, wj = get_wj_delta(file_harmonic_displacement)

    # weighted difference
    weighted_diff = np.repeat(mass, len(GS) // len(mass)) * (GS - ES)

    # weighted normal mode
    weights = np.repeat(np.repeat(mass, 3).reshape(
        1, -1), len(nm)//(3*len(mass)), axis=0).flatten()
    weighted_nm = (nm * weights).reshape(-1, len(mass)*3)
    weighted_nm /= np.linalg.norm(weighted_nm, axis=-1, keepdims=True)

    # last computation
    scalars = weighted_diff @ weighted_nm.T
    result = np.abs(scalars * np.sqrt(freqs*CM1_AU))

    # set order to print
    order_anharmonic = sorted(
        range(len(freqs)), key=lambda x: freqs[x], reverse=True)
    order_harmonic = sorted(
        range(len(wj)), key=lambda x: wj[x], reverse=True)

    # compute a relative difference between harm and anharm displ
    rel_diff = abs(
        (delta[order_harmonic] - result[order_anharmonic])/result[order_anharmonic]) * 100

    output = np.concatenate((freqs[order_anharmonic].reshape(-1, 1), result[order_anharmonic].reshape(-1, 1),
                             wj[order_harmonic].reshape(-1, 1), delta[order_harmonic].reshape(-1, 1), rel_diff.reshape(-1, 1)), axis=1)

    idx = np.logical_and(output[:, 1] >= 0.1, output[:, 3] >= 0.1)
    np.savetxt(file_output, output[idx],
               header=' Frequencies          & Anharmonic displacement| & Frequencies          & Harmonic displacement  & Relative difference %', delimiter=" ")
